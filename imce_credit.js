// Implements imce.hookOpSubmit().
imce.creditOpSubmit = function(dop) {
  if (imce.fopValidate('credit')) {
    imce.fopLoading('credit', true);
    jQuery.ajax(jQuery.extend(imce.fopSettings('credit'), {success: imce.creditResponse}));
  }
};

// Add hook.load.
imce.hooks.load.push(function() {
  jQuery('#edit-credit').click(function() {
    // Save new value in js var.
    var newVal = jQuery('#edit-credit-value').val();
    for (key in imce.selected) break;
    Drupal.settings.imce_credit.newCreditValue = {};
    Drupal.settings.imce_credit.newCreditValue[key] = newVal;
  });
  // Set click function for credit.
  imce.ops['credit'].func = imce.creditPrepare;
  // Add the credit info via js so as not to mess with validation.
  imceCreditAddCreditInfo();
});

imce.hooks.navigate.push(function() {
  // Add the credit info via js so as not to mess with validation.
  imceCreditAddCreditInfo();
});

// Populate the textbox with the current value.
imce.creditPrepare = function(show) {
  if (show) {
    var numSelectedFiles = 0;
    for (var fid in imce.selected) {
      numSelectedFiles++;
    }

    if (numSelectedFiles == 1) {
      var creditVal = imceCreditGetCredit(fid);
      if (creditVal) {
        jQuery('#edit-credit-value').val(creditVal);
      }
      else {
        jQuery('#edit-credit-value').val('');
      }
    }
    else if (numSelectedFiles == 0) {
      imce.setMessage(Drupal.t('A file must be selected.'), 'error');
      imce.opShrink('credit', 'hide');
    }
    else {
      imce.setMessage(Drupal.t('Only one file can be credited at a time.'), 'error');
      imce.opShrink('credit', 'hide');
    }
  }
};

// Custom response.
imce.creditResponse = function(response) {
  imce.processResponse(response);
  // Update the js variable.
  var newVal = Drupal.settings.imce_credit.newCreditValue;
  for (var index in newVal) {
    if (newVal.hasOwnProperty(index)) {
      Drupal.settings.imce_credit.creditValues[index] = newVal[index];
    }
  }

  // Refresh the current directory.
  jQuery.ajax(imce.navSet(imce.conf.dir, false));
  imce.opShrink('credit', 'fadeOut');
};

imceCreditGetCredit = function(filename) {
  if (typeof Drupal.settings.imce_credit !== 'undefined') {
    var creditValues = Drupal.settings.imce_credit.creditValues;
    if (creditValues.hasOwnProperty(filename)) {
      return creditValues[filename];
    }
  }

  return false;
};

imceCreditAddCreditInfo = function() {
  // Set click function for credit.
  imce.ops['credit'].func = imce.creditPrepare;

  // Add the credit info via js so as not to mess with validation.
  var files = imce.fids;
  for (var file in files) {
    if (files.hasOwnProperty(file)) {
      var creditVal = imceCreditGetCredit(file);
      if (creditVal) {
        var creditString = ' (Credit: ' + creditVal + ')';
        imce.fids[file].cells["0"].innerText += creditString;
      }
    }
  }
};
