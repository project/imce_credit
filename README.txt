
IMCE Image Credit
http://drupal.org/project/imce_credit
====================================

-- SUMMARY --

This module makes use of the file entity module to extend the IMCE module to allow you to add credit information to images. A new field is added to the image file type which can be edited either through the file edit as normal, or directly from within the IMCE browser.


-- REQUIREMENTS --

* IMCE: https://www.drupal.org/project/imce
* File Entity (fieldable files): https://www.drupal.org/project/file_entity


-- INSTALLATION --

* Install as usual.
* A new field will be created on install.


-- CONFIGURATION --

* Permissions: The ADD CREDIT TO IMAGES IMCE profile permission can be managed within the profile settings e.g. admin/config/media/imce/profile/edit/1
